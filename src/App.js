import React, { useEffect, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Board from './Components/Board';
import './App.css'

function App() {
  
  const [space, setSpace] = useState(['','','','','','','','',''])
  const [currentGameStatus, setCurrentGameStatus] = useState([])
  const [lastPlay, setLastPlay] = useState('')
  const [winner, setWinner] = useState('')
  useEffect(()=>{
    (async()=>{
      let getwinner = await checkWinner(currentGameStatus)
      if(getwinner !== null ){
        setWinner(getwinner)
      }
    })()
  },[currentGameStatus])

  console.log("currentGameStatus:::>>",currentGameStatus)
  
  const resetGame = ()=>{
    setSpace(['','','','','','','','',''])
    setCurrentGameStatus([])
    setLastPlay([])
    setWinner('')
  } 

  const checkWinner = async(currentGameStatus)=>{
    let winningArr = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ]
    for(let idx in winningArr){
      let arr = winningArr[idx]
      let [a,b,c] = arr

      if(currentGameStatus[a] && currentGameStatus[a] === currentGameStatus[b] && currentGameStatus[b] === currentGameStatus[c]){
        return currentGameStatus[a]
      }  
    }
    return null
  }
  return (
    <>
      <h1 style={{textAlign:'center'}}>Tic Tac Toe</h1>
      <div className='container main'>
        
        <div className='col-12' >  
          {winner !== '' && <h2 style={{textAlign:'center'}}>Winner of the Game :{winner}</h2>}
          {/* {currentGameStatus.length === 9  && <h2 style={{textAlign:'center'}}>Game Draw</h2>} */}
          <Board space={space} setSpace={setSpace} currentGameStatus={currentGameStatus} setCurrentGameStatus={setCurrentGameStatus} setLastPlay={setLastPlay} lastPlay={lastPlay} winner={winner}/>
        </div>
        <div className='mt-3'>
          <button className="btn btn-success mx-2" onClick={resetGame}>Reset</button>
        </div>
      </div>
    </>
  );
}

export default App;
