import React, { useEffect } from "react";
import './Board.css'
function Board({space,setSpace,currentGameStatus,setCurrentGameStatus,setLastPlay,lastPlay,winner}){
    console.log("currentGameStatus:::>>",currentGameStatus)

    const play = async(i)=>{
        if(currentGameStatus.length >0 && currentGameStatus[i] || winner){
            return;
        }else{
            let updatedSpace = [...space];
            if(currentGameStatus.length === 0){
                updatedSpace[i] = 'X'
            }else{
                if(lastPlay === 'X'){
                    updatedSpace[i] = 'O' 
                }else{
                    updatedSpace[i] = 'X'  
                }
            }
            console.log("this",i)
            setSpace(updatedSpace)
            setLastPlay(updatedSpace[i])
        }
    }

    useEffect(()=>{
        setCurrentGameStatus(space)
    },[space])
    return(
        <>
            <div className="col-3 main-board" >
                <div className="col-12 row first-row">
                    <div className='col-4 first-column' onClick={()=>play(0)}>
                        <span className="span-text" >{space[0]}</span>
                    </div>

                    <div className="col-4 second-column" onClick={()=>play(1)}>
                       <span className="span-text" >{space[1]}</span>
                    </div>

                    <div className="col-4 third-column" onClick={()=>play(2)}>
                        <span className="span-text" >{space[2]}</span>
                    </div>
                </div>

                <div className="col-12 row second-row">
                    <div className='col-4 first-column' onClick={()=>play(3)}>
                        <span className="span-text" >{space[3]}</span>
                    </div>

                    <div className="col-4 second-column" onClick={()=>play(4)}>
                        <span className="span-text" >{space[4]}</span>
                    </div>

                    <div className="col-4 third-column" onClick={()=>play(5)}>
                        <span className="span-text" >{space[5]}</span>
                    </div>
                </div>

                <div className="col-12 row third-row">
                    <div className='col-4 first-column' onClick={()=>play(6)}>
                        <span className="span-text" >{space[6]}</span>
                    </div>

                    <div className="col-4 second-column" onClick={()=>play(7)}>
                        <span className="span-text" >{space[7]}</span>
                    </div>

                    <div className="col-4 third-column" onClick={()=>play(8)}>
                        <span className="span-text" >{space[8]}</span>
                    </div>
                </div>

            </div>
        </>
    )
}
export default Board